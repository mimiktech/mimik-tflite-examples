# !/bin/bash
ARGUMENT_LIST=(
  "model_id"
  "classification"
  "score"
  "cap_file_path"
)

# read arguments
opts=$(getopt \
  --longoptions "$(printf "%s:," "${ARGUMENT_LIST[@]}")" \
  --name "$(basename "$0")" \
  --options "" \
  -- "$@"
)

eval set --$opts

while [[ $# -gt 0 ]]; do
  case "$1" in
    --model_id)
      MODELID=$2
      shift 2
      ;;

    --classification)
      CLASSIFICATION=$2
      shift 2
      ;;

    --score)
      SCORE=$2
      shift 2
      ;;

    --cap_file_path)
      CAP_FILE_PATH=$2
      shift 2
      ;;

    *)
      break
      ;;
  esac
done

echo "modelId=$MODELID, classficiation=$CLASSIFICATION, score=$SCORE, cap_file_path=$CAP_FILE_PATH"

#exit 200
