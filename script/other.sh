# !/bin/bash
ARGUMENT_LIST=(
  "model_id"
  "classification"
  "score"
)

# read arguments
opts=$(getopt \
  --longoptions "$(printf "%s:," "${ARGUMENT_LIST[@]}")" \
  --name "$(basename "$0")" \
  --options "" \
  -- "$@"
)

eval set --$opts

while [[ $# -gt 0 ]]; do
  case "$1" in
    --model_id)
      MODELID=$2
      shift 2
      ;;

    --classification)
      CLASSIFICATION=$2
      shift 2
      ;;

    --score)
      SCORE=$(echo $2*100 | bc)
      SCORE=$(echo ${SCORE%.*})
#      SCORE=$2
      shift 2
      ;;

    *)
      break
      ;;
  esac
done

# CACHE_FILE=/var/opt/mec-setup-poc/rp2-inferencing/starter/.cache/ml.log
echo "modelId=$MODELID, classficiation=$CLASSIFICATION, score=$SCORE"
# >> $CACHE_FILE

if [[ -n "${DF_NOTIFY_URL}" ]]; then
  template='{"jsonrpc": "2.0", "method": "machine_learning.on_object_detect", "params": {"modelId": "%s","score": %s,"classification": "%s"}}'
  json_string=$(printf "$template" "$MODELID" "$SCORE" "$CLASSIFICATION")

  if [ -z "$DF_CORRELATION_ID" ]; then
    DF_CORRELATION_ID=$(uuidgen)
  fi

  curl --request POST \
  --url "$DF_NOTIFY_URL" \
  --header "authorization: app $DF_APIKEY" \
  --header "x-correlation-id: $DF_CORRELATION_ID" \
  --header 'content-type: application/json' \
  --header 'user-agent: vscode-restclient' \
  --data "$json_string"
fi