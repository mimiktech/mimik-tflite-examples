#pragma once

#include <opencv2/opencv.hpp>
#include <vector>
#include <civetweb.h>
#include <mutex>
#include <atomic>

namespace mimik {
namespace ml {
using BufPtr = std::shared_ptr<std::vector<uchar>>;
class VideoHttpClient {
 public:
  BufPtr getBuf();

 public:
  struct mg_connection* conn;
  std::mutex listLock;
  std::list<BufPtr> bufList;
};

using VideoHttpClientPtr = std::shared_ptr<VideoHttpClient>;

class VideoHttpStream {
 public:
  static std::string captureFrame(const cv::Mat& frame,
                                 const std::string& capturePath);

 public:
  VideoHttpStream();
  ~VideoHttpStream();

  void pushFrame(const cv::Mat& frame);

  void listen(int port, const std::string& capturePath);
  void stop();

  bool isRunning();

  std::string newCaptureFilename();

  std::string get_video_stream_uri();
  std::string get_capture_uri();

  VideoHttpClientPtr addClient(struct mg_connection*);
  void removeClient(struct mg_connection*);

 private:
  bool runningStatus;
  struct mg_context* ctx;
  std::string capturePath;
  int listen_port = 0;

  ///
  std::atomic<int> clientSize;
  std::mutex clientLock;
  std::map<struct mg_connection*, VideoHttpClientPtr> clients;
};
}  // namespace ml
}  // namespace mimik