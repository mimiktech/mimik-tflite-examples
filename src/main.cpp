#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <dirent.h>

#include <opencv2/opencv.hpp>
#include <opencv2/dnn.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/ocl.hpp>

#include "MimikObjectDetector.hpp"
#include "VideoHttpStream.hpp"

#include <fstream>
#include <iostream>
#include <cmath>
#include <sstream>

#include <chrono>
#include <thread>

/*
 * daemonize.c
 * This example daemonizes a process, writes a few log messages,
 * sleeps 20 seconds and terminates afterwards.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>

static void skeleton_daemon() {
  pid_t pid;

  /* Fork off the parent process */
  pid = fork();

  /* An error occurred */
  if (pid < 0) exit(EXIT_FAILURE);

  /* Success: Let the parent terminate */
  if (pid > 0) exit(EXIT_SUCCESS);

  /* On success: The child process becomes session leader */
  if (setsid() < 0) exit(EXIT_FAILURE);

  /* Catch, ignore and handle signals */
  // TODO: Implement a working signal handler */
  signal(SIGCHLD, SIG_IGN);
  signal(SIGHUP, SIG_IGN);

  /* Fork off for the second time*/
  pid = fork();

  /* An error occurred */
  if (pid < 0) exit(EXIT_FAILURE);

  /* Success: Let the parent terminate */
  if (pid > 0) exit(EXIT_SUCCESS);

  /* Set new file permissions */
  umask(0);

  /* Change the working directory to the root directory */
  /* or another appropriated directory */
  // chdir("/");

  /* Close all open file descriptors */
  int x;
  for (x = sysconf(_SC_OPEN_MAX); x >= 0; x--) {
    close(x);
  }

  /* Open the log file */
  openlog("firstdaemon", LOG_PID, LOG_DAEMON);
}

using namespace std;

struct ML_Context {
  std::string execPath = "./script/default.sh";
  std::string modelId = "default";
  std::string sourcePath = "James.mp4";
  std::string modelPath = "default.tflite";
  std::string labelPath = "default.label";
  std::string capturePath = "./vcap";

  bool isImageSource = false;
  bool daemonize = false;
  bool display = true;
  bool imageDeleteAfterUse = false;
  int enable_workaround = 0;
  int unsigned delay = 1;  // in second
  int unsigned httpPort = 0;
};

inline bool ends_with(std::string const &value, std::string const &ending) {
  if (ending.size() > value.size()) return false;
  return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

static ML_Context getOps(int argc, char **argv) {
  ML_Context ml_context;
  int c;
  while ((c = getopt(argc, argv, "bndwji:m:x:s:l:p:a:z:")) != -1) {
    switch (c) {
      case 'j':
        ml_context.isImageSource = true;
        break;

      case 'b':
        ml_context.imageDeleteAfterUse = true;
        break;

      case 'p':  // modelPath
        ml_context.modelPath = optarg;
        break;

      case 'l':  // label
        ml_context.labelPath = optarg;
        break;

      case 'd':  // daemonize
        ml_context.daemonize = true;
        break;

      case 'i':  // video input
        ml_context.sourcePath = optarg;
        break;

      case 'x':  // script
        ml_context.execPath = optarg;
        break;

      case 'm':  // model id
        ml_context.modelId = optarg;
        break;

      case 's':  // delay
        ml_context.delay = atoi(optarg);
        break;

      case 'n':  // no display
        ml_context.display = false;
        break;
      case 'w':  // enable work around for rtsp
        ml_context.enable_workaround = 1;
        break;

      case 'a':  // enable http streaming with port
        ml_context.httpPort = strtoul(optarg, NULL, 0);
        break;

      case 'z':  // capture path;
        ml_context.capturePath = optarg;
        break;

      default:
        abort();
        break;
    }
  }

  return ml_context;
}

static bool dir_exists(const std::string &path) {
  DIR *dir = opendir(path.c_str());
  if (dir) {
    /* Directory exists. */
    closedir(dir);
    return true;
  } else if (ENOENT == errno) {
    /* Directory does not exist. */
  } else {
    /* opendir() failed for some other reason. */
  }

  return false;
}

// Function to create a directory
static int make_directory(const char *name) {
#ifdef _WIN32
  return _mkdir(name);  // Windows function
#else
  return mkdir(
      name, 0777);  // POSIX function, setting mode to 0777 allows rwx for all
#endif
}

// Recursive function to create a directory path
static void create_directories(const std::string &path) {
  char tmp[256];
  char *p = NULL;
  size_t len;

  snprintf(tmp, sizeof(tmp), "%s", path.c_str());
  len = strlen(tmp);
  if (tmp[len - 1] == '/') {
    tmp[len - 1] = 0;  // Remove trailing slash
  }

  // Create each parent directory
  for (p = tmp + 1; *p; p++) {
    if (*p == '/') {
      *p = 0;  // Temporarily truncate
      if (make_directory(tmp) != 0 && errno != EEXIST) {
        fprintf(stderr, "Error creating directory %s\n", tmp);
        return;
      }
      *p = '/';
    }
  }

  // Create the final directory
  if (make_directory(tmp) != 0 && errno != EEXIST) {
    fprintf(stderr, "Error creating directory %s\n", tmp);
  }
}

int main(int argc, char **argv) {
  float FPS[16] = {0.0};  // initialize all array to 0.0
  static unsigned int time_sec0 = 0;
  static int last_det_index = -1;

  mimik::ml::MimikObjectDetector mDetector;

  mimik::ml::VideoHttpStream mStream;

  ML_Context ml_context = getOps(argc, argv);

  if (ml_context.daemonize) {
    skeleton_daemon();
  }

  if (ml_context.httpPort) {
    create_directories(ml_context.capturePath);
    if (!dir_exists(ml_context.capturePath)) {
      cout << "cannot find: " << ml_context.capturePath << endl;
      return 0;
    }
    mStream.listen(ml_context.httpPort, ml_context.capturePath);

    cout << "video_stream uri: " << mStream.get_video_stream_uri() << endl;
    cout << "capture uri: " << mStream.get_capture_uri() << endl;
  }

  cv::Mat frame;
  chrono::steady_clock::time_point Tbegin, Tend;
  std::string errormsg;
  int hasError = mDetector.init(
      ml_context.modelPath, ml_context.labelPath, errormsg,
      [&frame,
       &ml_context](const std::vector<mimik::ml::DetectedContext> &list) {
        unsigned int time_sec = time(NULL);

        int found_index = -1;

        for (unsigned int i = 0; i < list.size(); i++) {
          if (found_index == -1) {
            found_index = i;
            continue;
          }

          auto &last = list[found_index];
          auto &current = list[i];

          if (current.score > last.score) {
            found_index = i;
          }
        }

        if (found_index != -1) {
          auto &ctx = list[found_index];
          float y1 = ctx.y1;
          float x1 = ctx.x1;
          float y2 = ctx.y2;
          float x2 = ctx.x2;

          cv::Rect rec((int)x1, (int)y1, (int)(x2 - x1), (int)(y2 - y1));
          cv::rectangle(frame, rec, cv::Scalar(0, 0, 255), 1, 8, 0);
          cv::putText(frame, cv::format("%s", ctx.classification.c_str()),
                      cv::Point(x1, y1 - 5), cv::FONT_HERSHEY_SIMPLEX, 0.5,
                      cv::Scalar(0, 0, 255), 1, 8, 0);

          if ((time_sec - time_sec0) < ml_context.delay) {
            return;
          }

          time_sec0 = time_sec;

          if (!ml_context.execPath.empty() &&
              (last_det_index != ctx.det_index)) {
            std::stringstream exe_cmd_string;
            if (ends_with(ml_context.execPath, ".sh")) {
              exe_cmd_string << "bash ";
            }

            exe_cmd_string << ml_context.execPath;
            exe_cmd_string << " --model_id " << ml_context.modelId;
            exe_cmd_string << " --classification " << ctx.classification;
            exe_cmd_string << " --score " << ctx.score;

            std::string cmd = exe_cmd_string.str();

            int ret = WEXITSTATUS(system(cmd.c_str()));

            if (ret == 200) {
              last_det_index = ctx.det_index;
              auto cap_file_path = mimik::ml::VideoHttpStream::captureFrame(
                  frame, ml_context.capturePath);
              exe_cmd_string << " --cap_file_path " << "\"" << cap_file_path
                             << "\"";

              std::string cmd = exe_cmd_string.str();
              system(cmd.c_str());
            } else {
              last_det_index = -1;
            }
          }
        }
      });

  if (hasError) {
    cout << errormsg << std::endl;
    exit(-1);
  }

  cv::VideoCapture cap;
  if (ml_context.isImageSource) {
    frame = cv::imread(ml_context.sourcePath);
  } else {
    cap = cv::VideoCapture(ml_context.sourcePath);

    if (!cap.isOpened()) {
      cerr << "ERROR: Unable to open the camera" << endl;
      return 0;
    }
  }

  if (ml_context.display) {
    cout << "Start grabbing, press ESC on Live window to terminate" << endl;
  }

  int i = 0;
  int Fcnt = 0;
  for (;;) {
    if (ml_context.isImageSource) {
    } else {
      cap >> frame;
      if (frame.empty()) {
        cerr << "ERROR: Unable to grab from the camera" << endl;

        if (ml_context.enable_workaround) {
          cap.release();
          cap = cv::VideoCapture(ml_context.sourcePath);
          if (!cap.isOpened()) {
            cerr << "ERROR: Unable to open the camera" << endl;
            return 0;
          }

          continue;
        }
        break;
      }
    }

    Tbegin = chrono::steady_clock::now();

    mDetector.detect(frame);

    Tend = chrono::steady_clock::now();

    // calculate frame rate
    float f =
        chrono::duration_cast<chrono::milliseconds>(Tend - Tbegin).count();
    if (f > 0.0) {
      FPS[((Fcnt++) & 0x0F)] = 1000.0 / f;
    }
    for (f = 0.0, i = 0; i < 16; i++) {
      f += FPS[i];
    }
    cv::putText(frame, cv::format("FPS %0.2f", f / 16), cv::Point(10, 20),
                cv::FONT_HERSHEY_SIMPLEX, 0.6, cv::Scalar(0, 0, 255));

    if (ml_context.httpPort) {
      mStream.pushFrame(frame);
    }

    if (ml_context.display) {
      cv::imshow(ml_context.modelId, frame);

      if (ml_context.isImageSource) {
        cv::waitKey(0);
      } else {
        char esc = cv::waitKey(5);
        if (esc == 27) break;
      }
    }

    if (ml_context.isImageSource) {
      if (ml_context.imageDeleteAfterUse) {
        remove(ml_context.sourcePath.c_str());
      }
      break;
    }
  }

  if (ml_context.httpPort) {
    mStream.stop();
  }

  cv::destroyAllWindows();

  return 0;
}
