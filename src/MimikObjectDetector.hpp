#pragma once

#include <opencv2/opencv.hpp>
#include <tensorflow/lite/interpreter.h>
#include <tensorflow/lite/kernels/register.h>
#include <tensorflow/lite/string_util.h>
#include <tensorflow/lite/model.h>

#include <vector>

namespace mimik {

namespace ml {

struct DetectedContext {
  float y1;
  float x1;
  float y2;
  float x2;
  float score;
  int det_index;
  std::string classification;
};

class MimikObjectDetector;

using MimikObjectDetectorInitCallback =
    std::function<void(const std::vector<DetectedContext>&)>;

class MimikObjectDetector {
 public:
  int init(const std::string& modelPath, const std::string& labelPath,
           std::string& errmsg,
           const MimikObjectDetectorInitCallback& on_detect_callback);
  void detect(cv::Mat& src);

 private:
  MimikObjectDetectorInitCallback on_detect_callback;

  std::unique_ptr<tflite::FlatBufferModel> model;
  std::vector<std::string> labels;
  std::unique_ptr<tflite::Interpreter> interpreter;

  int interpretor_input;
  int wanted_height;
  int wanted_width;
  int wanted_channels;
  int wanted_type;
};
}  // namespace ml
}  // namespace mimik