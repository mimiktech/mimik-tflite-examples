#include "VideoHttpStream.hpp"
#include <chrono>
#include <thread>
#include <sys/stat.h>
#include <nlohmann/json.hpp>

#define VIDEO_STREAM_URI "/video_stream"
#define CAPTURES_URI "/captures"
#define MB 1024 * 1024

using namespace mimik::ml;
using cppjson = nlohmann::json;

static std::string gen_random(const int len) {
  static const char alphanum[] =
      "0123456789"
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
      "abcdefghijklmnopqrstuvwxyz";
  std::string tmp_s;
  tmp_s.reserve(len);

  for (int i = 0; i < len; ++i) {
    tmp_s += alphanum[rand() % (sizeof(alphanum) - 1)];
  }

  return tmp_s;
}

static void save_file(const std::string& filename, BufPtr& buf) {
  FILE* pFile = fopen(filename.c_str(), "wb");
  fwrite(&(*buf.get())[0], 1, buf->size(), pFile);
  fclose(pFile);
}

static int video_stream_handler(struct mg_connection* conn, void* cbdata) {
  VideoHttpStream* stream = (VideoHttpStream*)cbdata;
  auto clientPtr = stream->addClient(conn);
  mg_printf(
      conn,
      "HTTP/1.1 200 OK\r\n"
      "Content-Type: multipart/x-mixed-replace; boundary=--jpgboundary\r\n"
      "\r\n");

  while (stream->isRunning()) {
    auto buf = clientPtr->getBuf();

    if (buf.get() == 0) {
      std::this_thread::sleep_for(std::chrono::milliseconds(3));
      continue;
    }

    mg_printf(conn, "--jpgboundary\r\n");
    mg_printf(conn, "Content-Type: image/jpeg\r\n");
    mg_printf(conn, "Content-length: %ld\r\n\r\n", buf->size());

    int ret = mg_write(conn, &(*buf.get())[0], buf->size());

    if (ret < 0) {
      break;
    }
  }

  stream->removeClient(conn);

  return 200;
}

static int captures_handler(struct mg_connection* conn, void* cbdata) {
  const mg_request_info* request = mg_get_request_info(conn);

  if (strcmp("POST", request->request_method) != 0) {
    mg_printf(conn, "HTTP/1.1 400 BAD REQUEST\r\n\r\n");
    return 400;
  }

  VideoHttpStream* stream = (VideoHttpStream*)cbdata;
  auto clientPtr = stream->addClient(conn);

  BufPtr buf;
  while (stream->isRunning()) {
    buf = clientPtr->getBuf();

    if (buf.get() == 0) {
      std::this_thread::sleep_for(std::chrono::milliseconds(3));
      continue;
    }

    break;
  }

  stream->removeClient(conn);

  if (buf.get() == 0) {
    mg_printf(conn, "HTTP/1.1 400 BAD REQUEST\r\n\r\n");
    return 400;
  }

  std::string filename = stream->newCaptureFilename();

  save_file(filename, buf);

  cppjson doc;
  doc["filename"] = filename;
  auto data = doc.dump();

  mg_printf(conn, "HTTP/1.1 201 CREATED\r\n");
  mg_printf(
      conn,
      "Access-Control-Allow-Methods: GET, POST, DELETE, PUT, OPTIONS\r\n");
  mg_printf(conn, "Access-Control-Allow-Origin: *\r\n");
  mg_printf(conn, "Content-Type: application/json\r\n");
  mg_printf(conn, "Content-Length: %ld\r\n\r\n", data.length());
  mg_printf(conn, "%s", data.c_str());

  return 201;
}

static void makedir(const std::string& dir) {
  mkdir(dir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
}

VideoHttpStream::VideoHttpStream()
    : runningStatus(false), ctx(0), clientSize(0) {}

VideoHttpStream::~VideoHttpStream() { stop(); }

void VideoHttpStream::pushFrame(const cv::Mat& frame) {
  if (!clientSize) {
    return;
  }

  auto buffer = std::make_shared<std::vector<uchar>>();
  buffer->resize(5 * MB);

  cv::imencode(".jpg", frame, *buffer);

  std::lock_guard<std::mutex> guard(clientLock);

  for (auto& c : clients) {
    // auto key = c.first;
    auto val = c.second;
    std::lock_guard<std::mutex> guard(val->listLock);

    if (val->bufList.size() > 10) {
      val->bufList.pop_front();
    }
    val->bufList.push_back(buffer);
  }
}

std::string VideoHttpStream::captureFrame(const cv::Mat& frame,
                                          const std::string& capturePath) {
  auto buffer = std::make_shared<std::vector<uchar>>();
  buffer->resize(5 * MB);

  cv::imencode(".jpg", frame, *buffer);

  auto filename = capturePath + "/" + gen_random(32) + ".jpg";

  save_file(filename, buffer);

  return filename;
}

void VideoHttpStream::listen(int port, const std::string& aCapturePath) {
  if (ctx) {
    return;
  }

  listen_port = port;
  char pathBuf[PATH_MAX]; /* PATH_MAX incudes the \0 so +1 is not required */
  capturePath = realpath(aCapturePath.c_str(), pathBuf);

  makedir(capturePath);

  std::string p = std::to_string(port);
  const char* options[] = {"listening_ports", p.c_str(), 0};

  struct mg_callbacks callbacks = {0};

  ctx = mg_start(&callbacks, 0, options);

  mg_set_request_handler(ctx, VIDEO_STREAM_URI, video_stream_handler,
                         (void*)this);

  mg_set_request_handler(ctx, CAPTURES_URI, captures_handler, (void*)this);

  runningStatus = true;
}

void VideoHttpStream::stop() {
  if (!ctx) {
    return;
  }

  runningStatus = false;

  mg_stop(ctx);

  ctx = NULL;
}

bool VideoHttpStream::isRunning() { return runningStatus; }

std::string VideoHttpStream::get_video_stream_uri() {
  return std::string("http://localhost:") + std::to_string(listen_port) +
         VIDEO_STREAM_URI;
}

std::string VideoHttpStream::get_capture_uri() {
  return std::string("http://localhost:") + std::to_string(listen_port) +
         CAPTURES_URI;
}

VideoHttpClientPtr VideoHttpStream::addClient(struct mg_connection* conn) {
  std::lock_guard<std::mutex> guard(clientLock);

  auto p = std::make_shared<VideoHttpClient>();
  p->conn = conn;
  clients[conn] = p;

  clientSize++;

  return p;
}

void VideoHttpStream::removeClient(struct mg_connection* conn) {
  std::lock_guard<std::mutex> guard(clientLock);

  clients.erase(conn);

  clientSize--;
}

std::string VideoHttpStream::newCaptureFilename() {
  return capturePath + "/" + gen_random(32) + ".jpg";
}

//////////////////////////////////////////
BufPtr VideoHttpClient::getBuf() {
  std::lock_guard<std::mutex> guard(listLock);
  if (bufList.empty()) {
    return {};
  }

  auto buf = *bufList.begin();
  bufList.pop_front();

  return buf;
}
