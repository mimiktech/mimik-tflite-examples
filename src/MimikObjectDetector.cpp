#include "MimikObjectDetector.hpp"

#include <fstream>

using namespace mimik::ml;

static bool getFileContent(const std::string &fileName,
                           std::vector<std::string> &labels) {
  // Open the File
  std::ifstream in(fileName.c_str());
  // Check if object is valid
  if (!in.is_open()) return false;

  std::string str;
  // Read the next line from File untill it reaches the end.
  while (std::getline(in, str)) {
    // Line contains string of length > 0 then save it in vector
    if (str.size() > 0) labels.push_back(str);
  }
  // Close The File
  in.close();
  return true;
}

inline bool ends_with(std::string const &value, std::string const &ending) {
  if (ending.size() > value.size()) return false;
  return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

int MimikObjectDetector::init(
    const std::string &modelPath, const std::string &labelPath,
    std::string &errmsg,
    const MimikObjectDetectorInitCallback &a_on_detect_callback) {
  on_detect_callback = a_on_detect_callback;
  bool result = getFileContent(labelPath, labels);
  if (!result) {
    errmsg = "loading labels failed";
    return -1;
  }

  // Load model
  model = tflite::FlatBufferModel::BuildFromFile(modelPath.c_str());

  // Build the interpreter
  tflite::ops::builtin::BuiltinOpResolver resolver;
  tflite::InterpreterBuilder(*model.get(), resolver)(&interpreter);

  interpreter->AllocateTensors();
  interpreter->SetAllowFp16PrecisionForFp32(true);
  interpreter->SetNumThreads(4);  // quad core

  /////////////////////////////////////////
  interpretor_input = interpreter->inputs()[0];

  wanted_type = interpreter->tensor(interpretor_input)->type;

  TfLiteIntArray *dims = interpreter->tensor(interpretor_input)->dims;
  wanted_height = dims->data[1];
  wanted_width = dims->data[2];
  wanted_channels = dims->data[3];

  /////////////////////////////////////////

  return 0;
}

void MimikObjectDetector::detect(cv::Mat &src) {
  cv::Mat image(wanted_height, wanted_width, src.type());
  cv::resize(src, image, image.size(), cv::INTER_CUBIC);

  if (wanted_type == kTfLiteFloat32) {
    // not quantized
    int cnls = image.type();
    if (cnls == CV_8UC1) {
      cv::cvtColor(image, image, cv::COLOR_GRAY2RGB);
    } else if (cnls == CV_8UC3) {
      cv::cvtColor(image, image, cv::COLOR_BGR2RGB);
    } else if (cnls == CV_8UC4) {
      cv::cvtColor(image, image, cv::COLOR_BGRA2RGB);
    }

    auto in16 = interpreter->typed_tensor<float>(interpretor_input);

    cv::Mat fimage;
    image.convertTo(fimage, CV_32FC3, 2.0 / 255.0, -1.0);

    memcpy(in16, fimage.data,
           sizeof(float) * wanted_height * wanted_width * wanted_channels);
  } else if (wanted_type == kTfLiteUInt8) {
    auto in8 = interpreter->typed_tensor<uint8_t>(interpretor_input);

    memcpy(in8, image.data,
           sizeof(uint8_t) * wanted_height * wanted_width * wanted_channels);
  }

  interpreter->Invoke();  // run your model

  const float *detection_locations =
      interpreter->tensor(interpreter->outputs()[0])->data.f;
  const float *detection_classes =
      interpreter->tensor(interpreter->outputs()[1])->data.f;
  const float *detection_scores =
      interpreter->tensor(interpreter->outputs()[2])->data.f;
  const int num_detections =
      *interpreter->tensor(interpreter->outputs()[3])->data.f;
  /////////////////////////////////////

  const float confidence_threshold = 0.5;

  int cam_width = src.cols;
  int cam_height = src.rows;

  std::vector<DetectedContext> detectedList;
  for (int i = 0; i < num_detections; i++) {
    if (detection_scores[i] > confidence_threshold) {
      int det_index = (int)detection_classes[i] + 1;
      float y1 = detection_locations[4 * i] * cam_height;
      float x1 = detection_locations[4 * i + 1] * cam_width;
      float y2 = detection_locations[4 * i + 2] * cam_height;
      float x2 = detection_locations[4 * i + 3] * cam_width;

      float score = detection_scores[i];
      std::string classification = labels[det_index];

      DetectedContext detected = {
          y1, x1, y2, x2, score, det_index, classification};

      detectedList.push_back(detected);
    }
  }

  if (!detectedList.empty()) {
    on_detect_callback(detectedList);
  }
}
